(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"intoComputer1_atlas_1", frames: [[0,0,2000,1207]]},
		{name:"intoComputer1_atlas_2", frames: [[0,0,2000,1125]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.Bitmap1 = function() {
	this.initialize(ss["intoComputer1_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap2 = function() {
	this.initialize(ss["intoComputer1_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.btnBackground = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.Bitmap1();
	this.instance.setTransform(0,0,0.2783,0.3652);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,556.7,410.9);


// stage content:
(lib.intoComputer1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [22];
	// timeline functions:
	this.frame_22 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(22).call(this.frame_22).wait(93));

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_2 = new cjs.Graphics().p("AgjAkQgOgPAAgVQAAgUAOgPQAPgPAUAAQAVAAAOAPQAQAPgBAUQABAVgQAPQgOAOgVAAQgUAAgPgOg");
	var mask_graphics_3 = new cjs.Graphics().p("AlQFMQiHiIAAjEQAAjDCHiMQCNiIDDAAQDEAACHCIQCNCMAADDQAADEiNCIQiHCMjEAAQjDAAiNiMg");
	var mask_graphics_4 = new cjs.Graphics().p("Ap7J0QkBkAAAl0QAAlyEBkKQEIkAFzAAQFzAAEAEAQEKEKAAFyQAAF0kKEAQkAEJlzAAQlzAAkIkJg");
	var mask_graphics_5 = new cjs.Graphics().p("AuoObQl5l5AAoiQAAohF5mGQGGl6IiAAQIiAAF6F6QGGGGAAIhQAAIimGF5Ql6GHoiAAQoiAAmGmHg");
	var mask_graphics_6 = new cjs.Graphics().p("AzUTEQnynyAArSQAArRHyoDQIDnyLRAAQLRAAHzHyQIDIDAALRQAALSoDHyQnzIDrRAAQrRAAoDoDg");
	var mask_graphics_7 = new cjs.Graphics().p("A4AXsQpsprAAuBQAAuAJsqAQKApsOAAAQOBAAJrJsQKAKAABOAQgBOBqAJrQprKAuBABQuAgBqAqAg");
	var mask_graphics_8 = new cjs.Graphics().p("A8scUQrlrkAAwwQAAwvLlr9QL9rlQvAAQQwAALkLlQL9L9AAQvQAAQwr9LkQrkL+wwAAQwvAAr9r+g");
	var mask_graphics_9 = new cjs.Graphics().p("EghZAg8QtdtdAAzfQAAzeNdt7QN7tdTeAAQTfAANdNdQN7N7AATeQAATft7NdQtdN7zfAAQzeAAt7t7g");
	var mask_graphics_10 = new cjs.Graphics().p("EgmFAlkQvWvWAA2OQAA2NPWv4QP4vWWNAAQWOAAPXPWQP3P4AAWNQAAWOv3PWQvXP42OAAQ2NAAv4v4g");
	var mask_graphics_11 = new cjs.Graphics().p("EgqxAqMQxPxOAA4+QAA48RPx1QR1xPY8AAQY9AARPRPQR1R1AAY8QAAY+x1ROQxPR149AAQ48AAx1x1g");
	var mask_graphics_12 = new cjs.Graphics().p("EgveAu0QzHzHAA7tQAA7sTHzyQTzzHbrgBQbsABTITHQTyTyAAbsQAAbtzyTHQzITz7sAAQ7rAAzzzzg");
	var mask_graphics_13 = new cjs.Graphics().p("Eg0KAzcQ1B1BAB+bQgB+bVB1vQVw1BeaAAQebAAVBVBQVwVvAAebQAAeb1wVBQ1BVw+bAAQ+aAA1w1wg");
	var mask_graphics_14 = new cjs.Graphics().p("Eg42A4FUgW6gW7AAAghKUAAAghKAW6gXsUAXsgW6AhKAAAUAhLAAAAW5AW6UAXtAXsAAAAhKUAAAAhKgXtAW7UgW5AXsghLAAAUghKAAAgXsgXsg");
	var mask_graphics_15 = new cjs.Graphics().p("Eg9iA8sUgYzgYyAAAgj6UAAAgj5AYzgZpUAZpgYzAj5AAAUAj6AAAAYzAYzUAZpAZpAAAAj5UAAAAj6gZpAYyUgYzAZqgj6AAAUgj5AAAgZpgZqg");
	var mask_graphics_16 = new cjs.Graphics().p("EhCPBBUUgargarAAAgmpUAAAgmoAargbnUAbngarAmoAAAUAmpAAAAasAarUAbmAbnAAAAmoUAAAAmpgbmAarUgasAbngmpAAAUgmoAAAgbngbng");
	var mask_graphics_17 = new cjs.Graphics().p("EhG7BF9UgckgclAAAgpYUAAAgpYAckgdjUAdkgckApXAAAUApYAAAAclAckUAdjAdjAAAApYUAAAApYgdjAclUgclAdkgpYAAAUgpXAAAgdkgdkg");
	var mask_graphics_18 = new cjs.Graphics().p("EhLnBKlUgeegeeAAAgsHUAAAgsGAeegfhUAfhgeeAsGAAAUAsHAAAAeeAeeUAfhAfhgABAsGUAABAsHgfhAeeUgeeAfhgsHAAAUgsGAAAgfhgfhg");
	var mask_graphics_19 = new cjs.Graphics().p("EhQUBPNUggWggWAAAgu3UAAAgu1AgWghfUAhfggWAu1AAAUAu2AAAAgXAgWUAheAhfAAAAu1UAAAAu3gheAgWUggXAhegu2AAAUgu1AAAghfgheg");
	var mask_graphics_20 = new cjs.Graphics().p("EhVABT0UgiPgiPAAAgxlUAAAgxlAiPgjaUAjcgiQAxkAAAUAxlAAAAiQAiQUAjbAjaAAAAxlUAAAAxlgjbAiPUgiQAjcgxlAAAUgxkAAAgjcgjcg");
	var mask_graphics_21 = new cjs.Graphics().p("EhZsBYdUgkIgkIAAAg0VUAAAg0UAkIglYUAlYgkIA0UAAAUA0VAAAAkIAkIUAlYAlYAAAA0UUAAAA0VglYAkIUgkIAlYg0VAAAUg0UAAAglYglYg");
	var mask_graphics_22 = new cjs.Graphics().p("EheYBdFUgmBgmBAAAg3EUAAAg3DAmBgnVUAnVgmBA3DgABUA3EAABAmBAmBUAnVAnVAAAA3DUAAAA3EgnVAmBUgmBAnVg3EAAAUg3DAAAgnVgnVg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(2).to({graphics:mask_graphics_2,x:634.95,y:486.65}).wait(1).to({graphics:mask_graphics_3,x:635.3,y:486.975}).wait(1).to({graphics:mask_graphics_4,x:635.65,y:487.3}).wait(1).to({graphics:mask_graphics_5,x:636,y:487.65}).wait(1).to({graphics:mask_graphics_6,x:636.375,y:487.975}).wait(1).to({graphics:mask_graphics_7,x:636.7,y:488.3}).wait(1).to({graphics:mask_graphics_8,x:637.05,y:488.625}).wait(1).to({graphics:mask_graphics_9,x:637.4,y:488.975}).wait(1).to({graphics:mask_graphics_10,x:637.75,y:489.3}).wait(1).to({graphics:mask_graphics_11,x:638.125,y:489.625}).wait(1).to({graphics:mask_graphics_12,x:638.475,y:490}).wait(1).to({graphics:mask_graphics_13,x:638.8,y:490.325}).wait(1).to({graphics:mask_graphics_14,x:639.175,y:490.65}).wait(1).to({graphics:mask_graphics_15,x:639.525,y:490.975}).wait(1).to({graphics:mask_graphics_16,x:639.875,y:491.325}).wait(1).to({graphics:mask_graphics_17,x:640.225,y:491.65}).wait(1).to({graphics:mask_graphics_18,x:640.55,y:491.975}).wait(1).to({graphics:mask_graphics_19,x:640.925,y:492.3}).wait(1).to({graphics:mask_graphics_20,x:641.275,y:492.65}).wait(1).to({graphics:mask_graphics_21,x:641.625,y:492.975}).wait(1).to({graphics:mask_graphics_22,x:641.975,y:493.3}).wait(93));

	// computer
	this.instance = new lib.btnBackground();
	this.instance.setTransform(639.95,456.85,2.2994,2.2221,0,0,0,278.3,205.6);
	this.instance._off = true;
	new cjs.ButtonHelper(this.instance, 0, 1, 1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(20).to({_off:true},1).wait(92));

	// Layer_1
	this.instance_1 = new lib.Bitmap2();
	this.instance_1.setTransform(0,0,0.64,0.8147);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({scaleX:0.2937,scaleY:0.2937,x:-1,y:1},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(639,456.5,641,526.9);
// library properties:
lib.properties = {
	id: '33013920BB0A15409F68DB1FA70AF910',
	width: 1280,
	height: 913,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/intoComputer1_atlas_1.png?1640778127774", id:"intoComputer1_atlas_1"},
		{src:"images/intoComputer1_atlas_2.png?1640778127774", id:"intoComputer1_atlas_2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['33013920BB0A15409F68DB1FA70AF910'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;