import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueIframe from 'vue-iframes'
// import Vuetify from 'vuetify';
// import 'vuetify/dist/vuetify.min.css';
// import Buefy from 'buefy';
// import 'buefy/dist/buefy.css';
// import VueFriendlyIframe from 'vue-friendly-iframe';

// Vue.use(VueFriendlyIframe);
Vue.use(VueIframe)
Vue.config.productionTip = false
    // Vue.use(Vuetify);
    // Vue.use(Buefy);


new Vue({
    store,
    render: h => h(App)
}).$mount('#app')